//
//  GameViewController.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 02/04/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit


extension SKNode {
	class func unarchive(from file : String) -> SKNode? {
		if let url = Bundle.main.url(forResource: file as String, withExtension: "sks") {
			
			do{
				
				let sceneData = try Data(contentsOf: url)
				
				//var sceneData = Data(bytesNoCopy: path, count: .DataReadingMappedIfSafe, deallocator: nil)!
				let archiver = NSKeyedUnarchiver(forReadingWith: sceneData)
				
				archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
				let scene = archiver.decodeObject(forKey: NSKeyedArchiveRootObjectKey) as! GameScene
				archiver.finishDecoding()
				return scene
			} catch {
				return nil
			}
		} else {
			return nil
		}
	}
}

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if let view = self.view as! SKView? {

            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
			view.showsPhysics = true
			
			let gameScene = GameScene(size: view.frame.size)
			gameScene.scaleMode = .aspectFill
			view.presentScene(gameScene)
			
        }
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
