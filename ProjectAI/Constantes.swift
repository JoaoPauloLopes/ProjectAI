//
//  Constantes.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 02/04/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit

let TIME_PER_FRAME = 0.1
var SPEED = 5
let MUTATION_RATE = 0.3
let CROSSOVER_RATE = 80.0
let QTDE_CHROMOSOMES = 1
let QTDE_INDIVIDUALS = 10
let QTDE_GENES = 3
let ELITISMO = true
