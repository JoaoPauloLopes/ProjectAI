//
//  BackgroundLayer.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 02/04/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit
import SpriteKit

class BackgroundLayer: SKNode {
	
	var background:SKSpriteNode!
	var origRunningBarPositionX = CGFloat(0)
	var maxBarX = CGFloat(0)
	
	
	init(image:UIImage,size:CGSize,position:CGPoint) {
		
		super.init()
		
		self.background = SKSpriteNode(texture: SKTexture(image: image))
		self.background.size.width = size.width * 2
		self.background.position = position
		self.background.zPosition = -1
		//self.background.setScale(1.5)
		self.background.texture?.filteringMode = .nearest
		
		
		self.origRunningBarPositionX = self.background.position.x
		self.maxBarX = self.background.size.width

		
		self.addChild(self.background)
		
		
	}
	
	
	func update(positionX:CGFloat,pointHide:CGFloat){
		
		
		
		if self.background.position.x <= pointHide{
			self.background.position.x = positionX
		}
		
		
		

	}
	
	func updateGround(){
		background.position.x -= CGFloat(SPEED)
	}
	
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	

}







