//
//  FileManagerLog.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 06/05/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit

class FileManagerLog: NSObject {
	
	
	static let PATH = "log.txt"
	
	
	static func write(text:String){
		
		
		let dir = FileManager.default.urls(for: FileManager.SearchPathDirectory.cachesDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first!
		let fileurl =  dir.appendingPathComponent(PATH)
		
		print(fileurl)
		
		let data = text.data(using: .utf8, allowLossyConversion: false)!
		
		if FileManager.default.fileExists(atPath: fileurl.path) {
			if let fileHandle = try? FileHandle(forUpdating: fileurl) {
				fileHandle.seekToEndOfFile()
				fileHandle.write(data)
				fileHandle.closeFile()
				
			}
		} else {
			try! data.write(to: fileurl, options: Data.WritingOptions.atomic)
		}
		
		
		
	}
	
	
	

}
