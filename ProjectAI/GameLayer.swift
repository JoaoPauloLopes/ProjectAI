//
//  GameLayer.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 02/04/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit
import SpriteKit

class GameLayer: SKNode {
	
	
	var player:Player!
	var groundPosition:CGFloat!
	var velocityY = CGFloat(0)
	let gravity = CGFloat(0.6)
	var onGround = true
	var jump = false
	var sizeScreen:CGSize!
    var groundObstacle = Obstacle()
	var groundObstacle2 = Obstacle()
	var groundObstacle3 = Obstacle()
    
	var newGameState:Bool = true
	
	var inputSensor:InputSensor!
	var population:Population!
	

	
	init(size:CGSize) {
		
		super.init()
		
		self.sizeScreen = size
		
		self.player = Player(images: ImagesAnimation.sharedInstance.run, timePerFrame: Float(TIME_PER_FRAME), position: CGPoint(x: size.width / 3,y: size.height * 0.15), size: CGSize(width: 150, height: 170))
		
		self.player.xScale = 0.5;
		self.player.yScale = 0.5;
		
		self.addChild(self.player)
		self.player.run(self.player.run(), withKey: "run")
		
		self.groundPosition = size.height * 0.15
		
		self.blockGenerator()
		
		
		
		//Sensor
		self.inputSensor = InputSensor()
		
		//Population
		self.population = Population(qtdeIndividuals: QTDE_INDIVIDUALS, qtdeChromosome: QTDE_CHROMOSOMES, qtdeGenes: QTDE_GENES)
		
		
		self.writeTxt()
		
		
		

		
	}
	
	func writeTxt()  {
		for i in population.individuals {
			
			FileManagerLog.write(text: "\n\nGeneration: "+String(GameState.sharedInstance.generation)+"\n"+i.chromosomes.description)
			//print("Generation: "+String(GameState.sharedInstance.generation)+"\n"+i.chromosomes.description)
			
		}
	}
	
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		//touchBeganJump()
		
		
	}
	
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		//touchEndedJump()
		
	}
	
	

	
	
	func touchBeganJump() {
		
		if self.onGround {
			self.velocityY = -18.0
			self.onGround = false
		}
		
		
		self.player.removeAction(forKey: "run")
		
		self.player.run(self.player.jump(), withKey: "jump")
		self.jump = true
	}
	
	func touchEndedJump() {
		
		if self.velocityY < -9.0 {
			self.velocityY = -9.0
		}
		
	}
	
	
	func didBeginContact(contact:SKPhysicsContact){

		
        GameState.sharedInstance.gameOver()
		
		
	}
	
	func update() {
		
		
		if(GameState.sharedInstance.alive){
			
			//self.inputSensor.update(gameLayer: self)
			

			self.inputSensor.update(distance: Double(self.distancePlayerBlock()), width: Double(self.blockCloser().size.width), speed: Double(SPEED))
			
			let sum = NeuralNetwork.neuron(input: self.inputSensor, individual: self.population.individuals[GameState.sharedInstance.individual.0 - 1])
			
			
			
			let activation = NeuralNetwork.activationFunctionSigmoid(output: sum)
			
			let i = self.population.individuals[GameState.sharedInstance.individual.0 - 1]
			
			/*
			print("Peso Distancia: "+String(i.chromosomes[0].genes[0].value))
			print("Peso width: "+String(i.chromosomes[0].genes[1].value))
			print("Peso Speed: "+String(i.chromosomes[0].genes[2].value))
			print("Width: "+String(Double(self.blockCloser().size.width)))
			print("Speed: "+String(Double(SPEED)))
			print("Distancia: "+String(Double(self.distancePlayerBlock())))
			print("Soma: "+String(sum))
			print("Ativação: "+String(activation))
			print("Fitness: "+String(i.chromosomes[0].fitness))
			*/
			print("Speed: "+String(Double(SPEED)))
			print("Fitness: "+String(i.chromosomes[0].fitness))
			print("Distancia: "+String(Double(self.distancePlayerBlock())))
			print("M: "+String(i.chromosomes[0].mutation))
			
			if (activation >= 0.6 && activation <= 0.8){
				
				self.touchBeganJump()
				
				let wait = SKAction.wait(forDuration:1.0)
				let action = SKAction.run {
					
					self.touchEndedJump()
					
					
					
				}
				run(SKAction.sequence([wait,action]))

				
			}
			
			
			
			self.velocityY += self.gravity
			
			self.updatePlayer()
			
			
			self.updateBlock()
			
			
			
			
			
			
			newGameState = true
		}
		else{
			
			
			if (newGameState){
				
				newGame()
				
				
			}

			
		}
		
	
		
	}
	
	func newGame(){
		
		SPEED = 5
		
		putPlayerInGround()
		
		self.groundObstacle.position = positionRandom()
		positionBlock(block: self.groundObstacle2, referenceBlockPosition: self.groundObstacle)
		positionBlock(block: self.groundObstacle3, referenceBlockPosition: self.groundObstacle2)
		
		GameState.sharedInstance.resetScore()
		
		let wait = SKAction.wait(forDuration:1.5)
		let action = SKAction.run {
			
			GameState.sharedInstance.alive = true
			
			
			
		}
		run(SKAction.sequence([wait,action]))
		
		if GameState.sharedInstance.individual.0 <  GameState.sharedInstance.individual.1 {
			
			GameState.sharedInstance.addindividual(increment: 1, maxIndividual: 10)
			
		}
		else{
			
			GameState.sharedInstance.resetIndividual()
			GameState.sharedInstance.addGeneration(increment: 1)
			//Nova população
			self.population.newPopulation()
			self.writeTxt()
			
		}
		
		self.newGameState = false
		
		
		
	}
	
	
	func putPlayerInGround(){
		
		self.player.position.y = self.groundPosition
		velocityY = 0.0
		self.onGround = true
		
	}
	
	
	func updatePlayer() {
		
		self.player.position.y -= velocityY
		self.player.position.x = self.sizeScreen.width / 3
		
		if self.player.position.y < self.groundPosition {
			
			putPlayerInGround()
			
			if(self.jump){
				self.player.removeAction(forKey: "jump")
				self.player.run(self.player.run(), withKey: "run")
				self.jump = false
			}
			
		}
		
		
		
	}
	
	func blockCloser() -> Obstacle {
		
		let position1 = self.groundObstacle.position.x - self.player.position.x
		let position2 = self.groundObstacle2.position.x - self.player.position.x
		let position3 = self.groundObstacle3.position.x - self.player.position.x
		
		if(position1 > 0 && position1 < position2 && position1 < position3){
			
			return self.groundObstacle
			
		}
		else{
			
			if(position2 > 0 && position2 < position3 && position2 < position1){
				
				return groundObstacle2
				
			}
			else{
				
				return groundObstacle3
				
			}
			
			
		}
		
		
		
	}
	
	func distancePlayerBlock() -> CGFloat {
		
		return (self.blockCloser().position.x - self.blockCloser().size.width / 2) - (self.player.position.x + self.player.size.width / 2)
		
	}
	
	
	func updateBlock(){
		
		updateBlock(block: self.groundObstacle, referenceBlockPosition: self.groundObstacle3)
		updateBlock(block: self.groundObstacle2, referenceBlockPosition: self.groundObstacle)
		updateBlock(block: self.groundObstacle3, referenceBlockPosition: self.groundObstacle2)
		
		
		
	}
	
	func updateBlock(block:Obstacle,referenceBlockPosition:Obstacle){
		
		block.position.x -= CGFloat(SPEED)
		
		if((block.position.x + block.size.width / 2 + self.player.size.width) < self.player.position.x && block.transposed == false) {
			
			GameState.sharedInstance.addScore(increment: 1)
			
			//Atualiza o fitness
			self.population.individuals[GameState.sharedInstance.individual.0 - 1].chromosomes[0].fitness = Double(GameState.sharedInstance.score)
			self.population.individuals[GameState.sharedInstance.individual.0 - 1].calculateFitness()
			
			if(GameState.sharedInstance.score % 10 == 0){
				SPEED += 1
			}
			block.transposed = true
		}
		
		if(block.position.x < 0){
			
			positionBlock(block: block, referenceBlockPosition: referenceBlockPosition)
			block.transposed = false
			
			
		}
		
		
		
	}
	
	func positionBlock(block:Obstacle,referenceBlockPosition:Obstacle){
	
		block.position.x = referenceBlockPosition.position.x + positionRandom().x
		
	}
	
	

	func createObstacle(){
        let image = #imageLiteral(resourceName: "block")
        self.groundObstacle = Obstacle(image:image, position: CGPoint(x: sizeScreen.width+50,y: sizeScreen.height * 0.15), size: CGSize(width: 150, height: 170))
        self.groundObstacle.physicsBody?.mass = 200000
		
		
		self.groundObstacle2 = Obstacle(image:image, position: CGPoint(x: groundObstacle.position.x + positionRandom().x,y: sizeScreen.height * 0.15), size: CGSize(width: 150, height: 170))
		self.groundObstacle2.physicsBody?.mass = 200000
		
		
		self.groundObstacle3 = Obstacle(image:image, position: CGPoint(x: groundObstacle2.position.x + positionRandom().x,y: sizeScreen.height * 0.15), size: CGSize(width: 150, height: 170))
		self.groundObstacle3.physicsBody?.mass = 200000
		
		
        self.addChild(groundObstacle)
		self.addChild(groundObstacle2)
		self.addChild(groundObstacle3)
		
		
	}
	
	func blockGenerator(){
        
        createObstacle()
        
	}
	
	func positionRandom() -> CGPoint {
		
		
		let randon = CGFloat(Int(arc4random_uniform(500))) + 10
		//print(randon)
		
		let position = CGPoint(x: sizeScreen.width + randon,y: sizeScreen.height * 0.15)
		
			
		return position
		
	}
	
	

}






















