//
//  NeuralNetwork.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 06/05/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit

class NeuralNetwork: NSObject {
	
	
	
	static func neuron(input:InputSensor,individual:Individual) -> Double {
		
		

		let valueDistance = individual.chromosomes[0].genes[0].value * input.distance
		let valueWidth =  individual.chromosomes[0].genes[1].value * input.width
		let valueSpeed = individual.chromosomes[0].genes[2].value * input.speed
		
		
		
		
		return (valueDistance + valueWidth + valueSpeed)
		
		
	}
	
	
	
	static func activationFunctionSigmoid(output:Double) -> Double {
		
		return 1.0 / (1.0 + exp(-output))
		
	}
	
	

}
