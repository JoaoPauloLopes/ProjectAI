//
//  Obstacle.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 02/04/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit
import SpriteKit

class Obstacle: GameObject {
	
	
	var transposed:Bool = false
	
	
	init(texture: SKTexture!) {
		
		super.init(texture: texture, color: UIColor.clear, size: texture.size())
	}
	
	override init(texture: SKTexture!, color: UIColor, size: CGSize) {
		super.init(texture: texture, color: color, size: size)
	}
	
	
	
	init(image:UIImage,position:CGPoint,size:CGSize) {
		
		let texture = SKTexture(image: image)
		super.init(texture: texture, color: UIColor.clear, size: texture.size())
		
		self.position = position
		self.physicsBody = self.generatePhysicBody(size: CGSize(width: size.width, height: size.height))
		

	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func generatePhysicBody(size: CGSize) -> SKPhysicsBody {
		
		let physicBody = SKPhysicsBody(rectangleOf: self.size)
		
		physicBody.categoryBitMask = ColliderType.block.rawValue
		physicBody.contactTestBitMask = ColliderType.player.rawValue
		physicBody.collisionBitMask = ColliderType.player.rawValue
		physicBody.mass = 20000
		physicBody.affectedByGravity = false
		physicBody.allowsRotation = false
		
		
		return physicBody;
		
		
	}
	


}
