//
//  Extensions.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 02/05/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit

extension Double{
	
	func randomDouble(min: Double, max: Double) -> Double {
		return (Double(arc4random()) / 0xFFFFFFFF) * (max - min) + min
	}

}
