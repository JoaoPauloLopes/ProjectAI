//
//  Gene.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 06/05/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit

class Gene: NSObject {
	
	var value:Double
	
	
	init(value:Double) {
		
		self.value = value
		
	}
	
	override init() {
		//self.value = Double().randomDouble(min: -100.0, max: 100.0)
		self.value = Double().randomDouble(min: -2.0, max: 2.0)
	}
	
	
	func mutation(){
		
			self.value += Double().randomDouble(min: -0.1, max: 0.1)

		
	}
	

	
	
	
}
