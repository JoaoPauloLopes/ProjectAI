//
//  GameObject.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 02/04/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit
import SpriteKit

class GameObject: SKSpriteNode {
	
	

	
	func generateTexture(name:String) -> SKTexture {
		
		return SKTexture(imageNamed: name)
		
	}
	
	func generateTexture(image:UIImage) -> SKTexture {
		
		return SKTexture(image: image)
		
	}
	
	func generatePhysicBody(size:CGSize) -> SKPhysicsBody{
		
		return SKPhysicsBody(rectangleOf: size)
		
		
	}
	
	func setBasicAttributes() {
		
		
	}
	
	func beginContac(node:SKNode, contact:SKPhysicsContact){
		
		
		
	}
	
	func endContact(node:SKNode, contact:SKPhysicsContact){
		
		
	}
	
	

}




























