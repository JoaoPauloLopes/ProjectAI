//
//  Population.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 06/05/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit

class Population: NSObject {
	
	var individuals:[Individual] = []
	var maxFitness:Double = 0.0
	var minFitness:Double = DBL_MAX
	

		
	init(qtdeIndividuals:Int,qtdeChromosome:Int,qtdeGenes:Int) {
			
			for _ in 1...qtdeIndividuals {
				
				self.individuals.append(Individual(qtdeChromosome: qtdeChromosome, qtdeGenes: qtdeGenes))
				
				
			}
		
		
		
	}
	
	init(individuals:[Individual]) {
		
		self.individuals = individuals
		
	}
	
	func calculateMaxeMinFitness() {
		
		for i in self.individuals {
			for c in i.chromosomes {
    
				if(c.fitness > self.maxFitness){
					
					self.maxFitness = c.fitness
					
				}
				
				if (c.fitness < self.minFitness) {
					
					self.minFitness = c.fitness
					
				}
				
				
			}
			
			
		}
		
		
	}
	
	func orderByFitness() {
		
		self.individuals = self.individuals.sorted { $0.fitness > $1.fitness }
		
		
	}
	
	func newPopulation() {
		
		self.calculateMaxeMinFitness()
		self.orderByFitness()
		
		var newIndividuals: [Individual] = []
		
		// Elitismo
		if(ELITISMO){
			
			newIndividuals.append(self.individuals[0])
			
		}
		
		while (newIndividuals.count < QTDE_INDIVIDUALS) {
			
			let parents = tournament(individuals: self.individuals)
			
			if (changeCrossover()) {
				
				let son = self.crossover(individual1: parents.0, individual2: parents.1)
			
				
				let randNum = Int(arc4random_uniform(UInt32(10)))
				
				if( randNum % 2 == 0){
					
					son.0.chromosomes[0].mutationChromosome()
					newIndividuals.append(son.0)
					
				}
				else{
					
					son.1.chromosomes[0].mutationChromosome()
					newIndividuals.append(son.1)
					
				}
				
				
			}
			else{
				
				if(!newIndividuals.contains(parents.0)){
					
					parents.0.chromosomes[0].mutationChromosome()
					newIndividuals.append(parents.0)
					
				}
				else{
					if(!newIndividuals.contains(parents.1)){
						
						parents.1.chromosomes[0].mutationChromosome()
						newIndividuals.append(parents.1)
						
					}
					
					
				}

			}
			
			
		}

	}
	
	func tournament (individuals:[Individual]) -> (Individual,Individual){
		
		var populationTournament:[Individual] = []
		
		for _ in 0...2 {
			
			let randNum = Int(arc4random_uniform(UInt32(QTDE_INDIVIDUALS)))
			populationTournament.append(individuals[randNum])
			
		}
		
		populationTournament = populationTournament.sorted { $0.fitness > $1.fitness }
		
		let winners = (populationTournament[0],populationTournament[1])
		
		return winners
		
	}
	
	
	
	func changeCrossover() -> Bool {
		
		let rand = Double().randomDouble(min: 0.0, max: 100.0)
		
		if (rand <= CROSSOVER_RATE) {
			
			return true
			
		}
		else{
			
			return false
			
		}
		
	}
	

	func crossover(individual1:Individual,individual2:Individual) -> (Individual,Individual) {
		
		let son = (Individual(qtdeChromosome: QTDE_CHROMOSOMES, qtdeGenes: QTDE_GENES),Individual(qtdeChromosome: QTDE_CHROMOSOMES, qtdeGenes: QTDE_GENES))
		
	
		for index in 0...QTDE_CHROMOSOMES - 1 {
			
			
			for index2 in 0...QTDE_GENES - 1 {
				
				
				let randNum = Int(arc4random_uniform(UInt32(10)))
				
				if( randNum % 2 == 0){
					
					son.0.chromosomes[index].genes[index2] = individual1.chromosomes[index].genes[index2]
					
					son.1.chromosomes[index].genes[index2] = individual2.chromosomes[index].genes[index2]
					
					
				}
				
			}
			
			
		}
		return son
	}

}
