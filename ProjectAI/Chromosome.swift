//
//  Chromosome.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 06/05/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit

class Chromosome: NSObject {
	
	// Position gene in array
	// 0 = distance
	// 1 = width
	// 2 = speed
	var genes:[Gene] = []
	var mutation:Bool = false
	var fitness:Double = 0.0
	
	
	override var description: String {
		
		let string = "\nFitness: "+String(fitness)+"\nDistance: "+String(genes[0].value)+"\nWidth: "+String(genes[1].value)+"\nSpeed: "+String(genes[2].value)
		
		return string
	}
	
	
	init(qtdGene:Int) {
		
		for _ in 1...qtdGene {
			
			self.genes.append(Gene())
			
			
		}
		
		
	}
	
	init(genes:[Gene]) {
		
		self.genes = genes
		
	}
	
	
	func addFitness(increment:Double){
		
		self.fitness += increment
		
	}
	
	
	func mutationChromosome() {
		
		if (changeMutation(probability: probabilityMutation())){
			
			self.fitness = 0
			
			self.mutation = true
			
			for g in self.genes {
				
				g.mutation()
					
				
			}
			
		}
		
		
	}
	
	func probabilityMutation() -> Double {
		
		// P (xi ) = 1 − (1 − pm )^n
		return 1 - pow(( 1 - MUTATION_RATE),Double(self.genes.count))
		
		
	}
	
	func changeMutation(probability:Double) -> Bool {
		
		let rand = Double().randomDouble(min: 0.0, max: 100.0)
		
		if (rand <= probability) {
			
			return true
			
		}
		else{
			
			return false
			
		}
		
	}
	
	
	
	

}
