//
//  GameScene.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 02/04/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate{
    
	var gameLayer:GameLayer!
	var backgroundLayer:BackgroundLayer!
	var backgroundLayer2:BackgroundLayer!
	var hudLayer:HudLayer!
	
	
	
	override init(size: CGSize) {
		
		super.init(size: size)
		
		let image = UIImage(named: "background")
		
		self.gameLayer = GameLayer(size: size)
		
		self.backgroundLayer = BackgroundLayer(image: image!,
		                                       size: size,
		                                       position: CGPoint(x: size.width , y: frame.origin.y))
		
		self.backgroundLayer2 = BackgroundLayer(image: image!,
		                                        size: size,
		                                        position: CGPoint(x: self.backgroundLayer.background.position.x + self.backgroundLayer.background.size.width, y: frame.origin.y))
		
		
		self.hudLayer = HudLayer(score: 0, generation: 0, individual: (1, 10), size: size, position: CGPoint(x: size.width * 0.50, y: size.height * 0.80))
		
		
		self.addChild(self.gameLayer)
		self.addChild(self.backgroundLayer)
		self.addChild(self.backgroundLayer2)
		self.addChild(self.hudLayer)
		
		self.physicsWorld.contactDelegate = self;
		self.physicsWorld.gravity = CGVector(dx: 0, dy: -2.0)
		
		
		
		
		
		
		
	}
	

	
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

    

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		self.gameLayer.touchesBegan(touches, with: event)
		
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		self.gameLayer.touchesMoved(touches, with: event)
		
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		self.gameLayer.touchesEnded(touches, with: event)
		
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		self.touchesCancelled(touches, with: event)
		
	}
	
	func didBegin(_ contact: SKPhysicsContact) {
		self.gameLayer.didBeginContact(contact: contact)
	}
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
		
		
		if (GameState.sharedInstance.alive) {
			
			self.hudLayer.score.text = String(GameState.sharedInstance.score)
			self.hudLayer.generation.text = String(GameState.sharedInstance.generation)
			self.hudLayer.individual.text = String(GameState.sharedInstance.individual.0)+"/"+String(GameState.sharedInstance.individual.1)
			
			
		}
		else{
			

            self.hudLayer.gameOver(size: self.frame.size, position: CGPoint(x: size.width * 0.50, y: size.height/2))
			
		}

				
		self.gameLayer.update()
		
		self.backgroundLayer.updateGround()
		self.backgroundLayer2.updateGround()
		
		
		self.backgroundLayer.update(positionX: self.backgroundLayer2.background.position.x + self.backgroundLayer2.background.size.width,pointHide: self.backgroundLayer.background.size.width * -1)
		self.backgroundLayer2.update(positionX: self.backgroundLayer.background.position.x + self.backgroundLayer.background.size.width,pointHide: self.backgroundLayer.background.size.width * -1)
	
    }
}
