//
//  GameState.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 02/04/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit

class GameState: NSObject {
	
	static let sharedInstance: GameState = GameState()
	
	var score:Int
	var alive:Bool
	
	var generation:Int
	var individual:(Int,Int)
	
	
	override init() {
		
		self.score = 0
		self.alive = true
		self.generation = 0
		self.individual = (1,QTDE_INDIVIDUALS)
		
		
	}
	
	
	func gameOver(){
		self.alive = false
	}
	
	func addScore(increment:Int) {
		
		self.score += increment
		
	}
	
	func addGeneration(increment:Int){
		
		self.generation += increment
		
	}
	
	func addindividual(increment:Int, maxIndividual:Int){
		
		self.individual.0 += increment
		self.individual.1 = maxIndividual
		
		
	}
	
	func resetIndividual(){
		
		self.individual.0 = 1
		
	}
	
	
	func resetScore() {
		self.score = 0
	}
	
	
	
	

}


