//
//  Player.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 02/04/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit
import SpriteKit

class Player: GameObject {
	
	var runState:SKAction!
	var jumpState:SKAction!

	
	init(texture: SKTexture!) {

		super.init(texture: texture, color: UIColor.clear, size: texture.size())
	}
	
	override init(texture: SKTexture!, color: UIColor, size: CGSize) {
		super.init(texture: texture, color: color, size: size)
	}


	
	init(images:[UIImage],timePerFrame:Float,position:CGPoint,size:CGSize) {
		
		let texture = SKTexture(image: images[0])
		super.init(texture: texture, color: UIColor.clear, size: texture.size())
		
		self.position = position
		self.initializeAnimationsRun(images: images, timePerFrame: timePerFrame)
		self.initializeAnimationsJump(images: ImagesAnimation.sharedInstance.jump, timePerFrame: 0.15)
		self.physicsBody = self.generatePhysicBody(size: CGSize(width: size.width, height: size.height))
		//self.physicsBody?.affectedByGravity = true

		
		
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func generatePhysicBody(size: CGSize) -> SKPhysicsBody {
		
		let physicBody = SKPhysicsBody(rectangleOf: self.size)
		
		physicBody.categoryBitMask = ColliderType.player.rawValue
		physicBody.contactTestBitMask = ColliderType.block.rawValue
		physicBody.collisionBitMask = ColliderType.block.rawValue
		physicBody.mass = 20000
		physicBody.affectedByGravity = false
		physicBody.allowsRotation = false
		
		
		return physicBody;

		
	}
	
	
	func loadAnimation(images:[UIImage],timePerFrame:Float) -> SKAction {
		
		var textures:[SKTexture] = []
		
		
		for image in images {
			
			textures.append(SKTexture(image: image))
			
			
		}
		
		let action = SKAction.animate(with: textures, timePerFrame: TimeInterval(timePerFrame))
		
		return action;
		
		
	}
	
	
	
	func initializeAnimationsRun(images:[UIImage],timePerFrame:Float){
		
		self.runState = self.loadAnimation(images: images, timePerFrame: timePerFrame)
		
	}
	
	func initializeAnimationsJump(images:[UIImage],timePerFrame:Float){
		
		self.jumpState = self.loadAnimation(images: images, timePerFrame: timePerFrame)
		
	}
	
	func run() -> SKAction {
		
		return SKAction.repeatForever(self.runState)
		
	}
	
	func jump() -> SKAction {
		return SKAction.repeat(self.jumpState, count: 1)
	}


}









