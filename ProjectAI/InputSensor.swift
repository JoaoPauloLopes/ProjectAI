//
//  InputSensor.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 06/05/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit

class InputSensor: NSObject {
	
	
	var distance:Double!
	var width:Double!
	var speed:Double!
	
	
	override init() {
		super.init()
		
		self.distance = 0
		self.width = 0
		self.speed = 0

	}
	
	
	
	func update(gameLayer:GameLayer) {
		
		
		self.distance = Double(gameLayer.distancePlayerBlock())
		self.width = Double(gameLayer.blockCloser().size.width)
		self.speed = Double(gameLayer.speed)
		
		
	}
	
	
	func update(distance:Double,width:Double,speed:Double) {
		
		
		self.distance = distance
		self.width = width
		self.speed = speed
		
		
	}
	
	

}
