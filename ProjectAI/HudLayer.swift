//
//  HudLayer.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 02/04/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit
import SpriteKit

class HudLayer: SKNode {
	
	
	var score:SKLabelNode!
	var generation:SKLabelNode!
	var individual:SKLabelNode!
	var alive:Bool!
	
	
	init(score:Int,generation:Int,individual:(Int,Int),size:CGSize,position:CGPoint) {
		
		super.init()
		
		self.score = SKLabelNode(text: String(score))
		self.score.fontSize = 50
		self.score.fontColor = UIColor.white
		self.score.position = position
		self.score.position.x = position.x + 100
		self.addChild(self.score)
		
		self.generation = SKLabelNode(text: String(generation))
		self.generation.fontSize = 40
		self.generation.fontColor = UIColor.white
		self.generation.position = position
		self.generation.position.x = position.x - 200
		self.addChild(self.generation)
		
		self.individual = SKLabelNode(text: String(individual.0)+"/"+String(individual.1))
		self.individual.fontSize = 40
		self.individual.fontColor = UIColor.white
		self.individual.position = position
		self.individual.position.x = position.x - 100
		self.addChild(self.individual)
		
		
		self.alive = true
		

		
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func gameOver(size:CGSize,position:CGPoint){

		
		self.score.text = "Game Over"
		self.alive = false


	}
	
	
	

}
