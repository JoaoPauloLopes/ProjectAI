//
//  PhysicsCategories.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 02/04/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit

enum ColliderType:UInt32 {
	case player = 1
	case block = 2
}
