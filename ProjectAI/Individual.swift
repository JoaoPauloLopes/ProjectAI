//
//  Individual.swift
//  ProjectAI
//
//  Created by Joao Paulo Lopes da Silva on 06/05/17.
//  Copyright © 2017 Joao Paulo Lopes da Silva. All rights reserved.
//

import UIKit

class Individual: NSObject {
	
	
	var chromosomes:[Chromosome] = []
	var fitness:Double = 0.0
	
	
	init(qtdeChromosome:Int,qtdeGenes:Int) {
		
		for _ in 1...qtdeChromosome {
			
			self.chromosomes.append(Chromosome(qtdGene: qtdeGenes))
			
			
		}

	}
	
	init(chromosomes:[Chromosome]) {
		
		self.chromosomes = chromosomes
		
	}
	
	override init() {
		super.init()
		
	}
	
	func calculateFitness() {
		
		self.fitness = 0
		for c in self.chromosomes {
			
			self.fitness += c.fitness
			
		}
		
		
	}
	

}
